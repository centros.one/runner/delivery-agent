# Author: Antony Goetzschel <ago@ccsolutions.io>
# Date: 10.11.20
# Terraform Version: 0.13.2

resource "azurerm_public_ip" "pip-aks-dev-weu-01" {
  name                         = "pip-dev-weu-01"
  location                     = var.location
  resource_group_name          = "MC_${azurerm_kubernetes_cluster.aks-dev-stage.resource_group_name}_${azurerm_kubernetes_cluster.aks-dev-stage.name}_${azurerm_kubernetes_cluster.aks-dev-stage.location}"
  allocation_method            = "Static"
  sku                          = "Standard"
  domain_name_label            = "ingressaksdevstage01"

  tags = {
    environment = local.environment
  }
}