# Author: Antony Goetzschel <ago@ccsolutions.io>
# Date: 10.11.20
# Terraform Version: 0.13.2

resource "azurerm_virtual_network" "vnet-dev-stage" {
  name = "vnet-${local.name}"
  address_space = [
    local.network_cidr]
  location = local.location

  subnet {
    name = "snet-${local.name}"
    address_prefix = cidrsubnet(local.network_cidr, 8, 1)
  }

  resource_group_name = local.resource_group
  tags = {
    environment = local.environment
  }
}