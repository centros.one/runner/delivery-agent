# Author: Antony Goetzschel <ago@ccsolutions.io>
# Date: 10.11.20
# Terraform Version: 0.13.2

resource "tls_private_key" "ssh-key-pair" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "azurerm_key_vault_secret" "ssh-key-public" {
  name         = "ssh-key-public"
  value        = tls_private_key.ssh-key-pair.public_key_openssh
  key_vault_id = azurerm_key_vault.keyvault.id

  tags = {
    environment = local.environment
  }
}

resource "azurerm_key_vault_secret" "ssh-key-private" {
  name         = "ssh-key-private"
  value        = tls_private_key.ssh-key-pair.private_key_pem
  key_vault_id = azurerm_key_vault.keyvault.id

  tags = {
    environment = local.environment
  }
}
