# Author: Antony Goetzschel <ago@ccsolutions.io>
# Date: 10.11.20
# Terraform Version: 0.13.2

data "azurerm_subnet" "akspool" {
  name                 = "snet-${local.name}"
  virtual_network_name = azurerm_virtual_network.vnet-dev-stage.name
  resource_group_name  = local.resource_group
}

resource "azurerm_kubernetes_cluster" "aks-dev-stage" {
  name = "aks-dev-stage"
  location = var.location
  kubernetes_version = "1.19.0"
  resource_group_name = local.resource_group
  dns_prefix = "aksdevstage"

  linux_profile {
    admin_username = "azureuser"
    ssh_key {
      key_data = azurerm_key_vault_secret.ssh-key-public.value
    }
  }

  default_node_pool {
    name            = "defaultpool"
    node_count      = local.aks_node_count
    vm_size         = local.aks_vm_size
    os_disk_size_gb = local.os_disk_size_gb
    vnet_subnet_id  = data.azurerm_subnet.akspool.id
  }

  service_principal {
    client_id = "e68ec5b4-e21a-492d-b30b-74b6993358f3"
    client_secret = "-Q6L6.c4C-a24Kq35XszuL_eqZ_f3JaMPa"
  }

  network_profile {
    network_plugin     = "kubenet"
    network_policy     = "calico"
  }

  tags = {
    environment = local.environment
  }

  depends_on = [azurerm_virtual_network.vnet-dev-stage]
}

resource "azurerm_key_vault_secret" "aks-k8s-config" {
  name         = "k8s-config-${local.name}"
  value        = azurerm_kubernetes_cluster.aks-dev-stage.kube_config_raw
  key_vault_id = azurerm_key_vault.keyvault.id

  tags = {
    environment = "Dev"
  }

  depends_on = [azurerm_kubernetes_cluster.aks-dev-stage]
}

resource "azurerm_key_vault_secret" "aks-k8s-fqdn" {
  name         = "k8s-fqdn"
  value        = azurerm_kubernetes_cluster.aks-dev-stage.fqdn
  key_vault_id = azurerm_key_vault.keyvault.id

  tags = {
    environment = local.environment
  }

  depends_on = [azurerm_kubernetes_cluster.aks-dev-stage]
}

# Initialize Helm
provider "helm" {

  kubernetes {
    host                   = azurerm_kubernetes_cluster.aks-dev-stage.kube_config[0].host
    client_certificate     = base64decode(azurerm_kubernetes_cluster.aks-dev-stage.kube_config.0.client_certificate)
    client_key             = base64decode(azurerm_kubernetes_cluster.aks-dev-stage.kube_config.0.client_key)
    cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.aks-dev-stage.kube_config.0.cluster_ca_certificate)
  }
}

# Install Nginx Ingress using Helm Chart
resource "helm_release" "nginx_ingress" {
  name              = "nginx-ingress"
  namespace         = "nginx-ingress"
  create_namespace  = true
  repository        = "https://kubernetes-charts.storage.googleapis.com"
  chart             = "nginx-ingress"

  set {
    name  = "controller.replicaCount"
    value = local.aks_node_count
  }

  set {
    name  = "controller.nodeSelector.kubernetes\\.io/os"
    value = "linux"
  }

  set {
    name  = "defaultBackend.nodeSelector.kubernetes\\.io/os"
    value = "linux"
  }

  set {
    name  = "controller.service.loadBalancerIP"
    value = azurerm_public_ip.pip-aks-dev-weu-01.ip_address
  }

  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/azure-dns-label-name"
    value = azurerm_public_ip.pip-aks-dev-weu-01.domain_name_label

  }
  depends_on = [azurerm_kubernetes_cluster.aks-dev-stage, azurerm_public_ip.pip-aks-dev-weu-01]
}

# Install Letsencrypt Certificate Manager
resource "helm_release" "cert_manager" {
  name              = "cert-manager"
  namespace         = "cert-manager"
  create_namespace  = true
  repository        = "https://charts.jetstack.io"
  chart             = "cert-manager"
  version           = "v0.16.1"

  set {
    name  = "installCRDs"
    value = true
  }

  set {
    name  = "nodeSelector.kubernetes\\.io/os"
    value = "linux"
  }

  depends_on = [azurerm_kubernetes_cluster.aks-dev-stage]
}